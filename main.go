package main

import (
	"strconv"

	"log"

	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gorilla/mux"
)

func hello(w http.ResponseWriter, r *http.Request) {

	w.Write([]byte("hello"))
}

func addTwpNums(w http.ResponseWriter, r *http.Request) {

	numOnes, ok := r.URL.Query()["numOne"]
	if !ok {
		w.WriteHeader(404)
		w.Write([]byte("something wrong"))
	}
	numTwos, ok := r.URL.Query()["numTwo"]

	numOne, err := strconv.Atoi(numOnes[0])

	numTwo, err := strconv.Atoi(numTwos[0])
	if err != nil {
		w.Write([]byte("somthing wrong...."))
	}

	sum := numOne + numTwo
	newsum := strconv.Itoa(sum)
	w.Write([]byte(newsum))

}

func calcDate(w http.ResponseWriter, r *http.Request) {
	v := r.URL.Query()

	days, _ := strconv.Atoi(v.Get("days"))
	hours := time.Duration(days) * 24
	newdate := time.Now().Add(hours * time.Hour).String()

	w.Write([]byte(newdate))

}

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/hello", hello).Methods(http.MethodGet)
	router.HandleFunc("/add", addTwpNums).Queries(
		"numOne", "{numOne}",
		"numTwo", "{numTwo}").Methods(http.MethodGet)
	router.HandleFunc("/calcDate", calcDate).Methods(http.MethodGet)

	srv := &http.Server{
		Addr:    ":8080",
		Handler: router,
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	log.Print("Server Started")

	<-done

	log.Print("Server Exited Properly")
}
